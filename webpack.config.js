const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    main: [
      path.resolve(__dirname, './src/index.ts'),
      path.resolve(__dirname, './src/assets/scss/index.scss'),
      path.resolve(__dirname, './src/assets/scss/demo.scss'),
    ]
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'js/[name].js',
    clean: true
  },
  devServer: {
    hot: 'only',
    static: path.resolve(__dirname, 'src'),
  },
  optimization: {
    runtimeChunk: 'single'
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          },
          'sass-loader'
        ],
      },
      {
        test: /\.tsx?$/,
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.(js)$/,
        use: ['babel-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.html$/i,
        loader: 'html-loader',
      }
    ]
  },
  resolve: {
    modules: [__dirname, 'node_modules'],
    extensions: ['.tsx', '.ts', '.js'],
  },
  plugins: [
    new HtmlWebpackPlugin(
      {
        template: 'src/index.html'
      }
    ),
    new MiniCssExtractPlugin(
      {
        filename: 'css/style.css'
      }
    )
  ]
};